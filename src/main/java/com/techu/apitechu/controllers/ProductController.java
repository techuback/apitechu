package com.techu.apitechu.controllers;

import com.techu.apitechu.ApitechuApplication;
import com.techu.apitechu.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductController {
    static final String API_BASE_URL = "/apitechu/v1";

    @GetMapping(API_BASE_URL + "/products")
    public ArrayList<ProductModel> getProducts() {
        System.out.println("getProducts");

        return ApitechuApplication.productModels;
    }

    @GetMapping(API_BASE_URL + "/products/{id}")
    public ProductModel getProductById(@PathVariable String id) {
        System.out.println("getProductBtId");
        System.out.println("id es " + id);

        ProductModel result = new ProductModel();

        for (ProductModel product : ApitechuApplication.productModels) {
            if (product.getId().equals(id)) {
                result = product;
            }
        }

        return result;
    }

    @PostMapping(API_BASE_URL + "/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct) {
        System.out.println("createProduct");
        System.out.println(String.format("Alta producto %s, %s, %f", newProduct.getId(),newProduct.getDesc(),newProduct.getPrice()));

        //TODO aca debemos persistir a BBDD
        ApitechuApplication.productModels.add(newProduct);

        return newProduct;
    }

    @PutMapping(API_BASE_URL + "/products/{id}")
    public ProductModel updateProduct(@RequestBody ProductModel product, @PathVariable String id) {
        System.out.println("updateProduct");
        System.out.println(String.format("Actualización producto %s, %s, %f", product.getId(),product.getDesc(),product.getPrice()));

        ProductModel result = new ProductModel();

        for (ProductModel productInList : ApitechuApplication.productModels) {
            if (productInList.getId().equals(id)) {
                productInList.setId(product.getId());
                productInList.setDesc(product.getDesc());
                productInList.setPrice(product.getPrice());
            }
        }
        return product;
    }

    @DeleteMapping(API_BASE_URL + "/products/{id}")
    public ProductModel deleteProduct(@PathVariable String id) {
        System.out.println("deleteProduct");
        System.out.println("a boorrar id es " + id);

        ProductModel result = new ProductModel();
        boolean foundProduct = false;

        for (ProductModel productInList : ApitechuApplication.productModels) {
            if (productInList.getId().equals(id)) {
                System.out.println("Prod encontrado");
                foundProduct = true;
                result = productInList;
            }
        }

        if (foundProduct == true) {
            System.out.println("borrando producto");
            ApitechuApplication.productModels.remove(result);
        }

        return result;
    }

    /**
     * Actualización de parte del producto
     * @param id    id del producto a modificar
     * @param body  en el body de la peticion debe venir los atributos a modificar
     * */
    @PatchMapping(API_BASE_URL + "/products/{id}")
    public ProductModel updateProductAttribute(@RequestBody ProductModel product,@PathVariable String id) {
        System.out.println("updateProductAttribute");
        System.out.println("id producto: " + id);

        ProductModel result = new ProductModel();
        boolean foundProduct = false;

        for (ProductModel productInList : ApitechuApplication.productModels) {
            if (productInList.getId().equals(id)) {
                System.out.println("**Producto encontrado**");
                foundProduct = true;

                if (product.getDesc() != null && !product.getDesc().equals("") ) {
                    System.out.println("**Actualizando descripcion:" + productInList.getDesc());
                    productInList.setDesc(product.getDesc());
                }
                if (product.getPrice() > 0) {
                    System.out.println("**Actualizando precio:" + productInList.getPrice());
                    productInList.setPrice(product.getPrice());
                }
                result = productInList;
                break;
            }
        }
        if (foundProduct == false) {
            System.out.println("**Producto NO EXISTE**");
        }

        return result;
    }
}
