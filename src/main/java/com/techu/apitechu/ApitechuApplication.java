package com.techu.apitechu;

import com.techu.apitechu.models.ProductModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@SpringBootApplication

@RestController
public class ApitechuApplication {

	public static ArrayList<ProductModel> productModels;

	public static void main(String[] args) {
		SpringApplication.run(ApitechuApplication.class, args);

		ApitechuApplication.productModels = ApitechuApplication.getTestData();
	}

	private static ArrayList<ProductModel> getTestData() {
		ArrayList<ProductModel> productModels = new ArrayList<>();

		productModels.add(
				new ProductModel("1","iPhone 13",899)
		);
		productModels.add(
				new ProductModel("2","iPhone 13 Max",1099)
		);
		productModels.add(
				new ProductModel("3","iPhone 12 Mini",699)
		);
		productModels.add(
				new ProductModel("4","MacBook Air M1",1099)
		);
		productModels.add(
				new ProductModel("5","AirPods Max",599)
		);

		return productModels;
	}
}
